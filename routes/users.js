var express = require('express');
var router = express.Router();
var groupsController = require('../controller/groupcontroller');

router.get('/',groupsController.getAll);

module.exports = router;
