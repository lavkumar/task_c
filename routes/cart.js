var express = require('express');
var router = express.Router();
const groupsController=require('../controller/groupcontroller')


router.get('/groups',groupsController.getgroups)
router.get('/groups/:GroupId/products',groupsController.activeProducts)
router.get('/groups/products/:id',groupsController.productsById)



router.get('/',groupsController.getAll);

module.exports = router;
