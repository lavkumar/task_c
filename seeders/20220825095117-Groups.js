'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Groups', [{
      Name: "Group1",
      description: "Group1",
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
    {
      Name: "Group2",
      description: "Group2",
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
    {
      Name: "Group3",
      description: "DGroup3",
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
  ]);

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */       
    await queryInterface.bulkDelete('Groups', null, {});
    


  }
};
