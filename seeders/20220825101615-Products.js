'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Products', [{
      Name: "product1",
      description: "product1",
      image_url: "https://image1",
      CategoryId: 1,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()

    },
    {
      Name: "product2",
      description: "product2",
      image_url: "https://image2",
      CategoryId: 1,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      Name: "product3",
      description: "product3",
      image_url: "https://image3",
      CategoryId: 2,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      Name: "product4",
      description: "product4",
      image_url: "https://image4",
      CategoryId: 2,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()

    }
    ])

  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Products', null, {});
  }
};
