'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Categories', [{
      Name: "Categories1",
      description:"categories",
      GroupId: 1,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
  {
    Name: "Categories2",
    description:"categories",
    GroupId: 2,
    isActive: true,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    Name: "Categories3",
    description:"categories",
    GroupId: 2,
    isActive: true,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    Name: "Categories4",
    description:"categories",
    GroupId: 1,
    isActive: true,
    createdAt: new Date(),
    updatedAt: new Date(),

  }
])

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Categories', null, {});
  }
};
