var Model = require('../models');

//getting all details of Groups,Products,Categories
module.exports.getAll = (req, res) => {
    Model.Groups.findAll({
        where:{},
        include: [{
            model: Model.Categories,
            where:{},
            include:[ { model: Model.Products }]
        }]
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
//getting all details of Groups who is active
module.exports.getgroups = (req, res) => {
    Model.Groups.findAll({
        where: { isActive: 'true' }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

//Get all Active products grouped with categories(only active with name and description)

module.exports.activeProducts = (req, res) => {
    const id = req.params.GroupId;
    Model.Groups.findAll({
        attributes: ['Name'], where: { isActive: 'true', id: id },
        include: {
            model: Model.Categories,
            where: { isActive: 'true' },
            attributes: ['Name'],
            include: {
                model: Model.Products,
                where: { isActive: 'true' },
                attributes: ['Name', 'description']
            }
        }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};



//Getting products by id

module.exports.productsById = (req, res) => {
    const id = req.params.id;
    Model.Groups.findAll({
        where: { isActive: "true" },
        attributes: ['Name'],
        include: {
            model: Model.Categories,
            where: { isActive: "true" },
            attributes: ['Name'],
            include: {
                model: Model.Products,
                where: {isActive:"true",id:id},
                

            }
        }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

