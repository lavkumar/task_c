'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
     // Products.belongsTo(models.Categories);
      // define association here
    }
  }
  Products.init({
    Name: DataTypes.STRING,
    description: DataTypes.STRING,
    image_url: DataTypes.STRING,
    CategoryId: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Products',
  });

  Products.associate = function (models) {
    Products.belongsTo(models.Categories);
  }

  return Products;
};