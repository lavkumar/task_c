const models =require('../models/categories.js')

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Groups extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
    
        //Groups.hasMany(models.Categories);
        Groups.hasMany(models.Categories);
      
      // define association here
    }
  }
  Groups.init({
    Name: DataTypes.STRING,
    description: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Groups',
  });

  
    //Groups.hasMany(models.Categories);
    
  
  return Groups;
};